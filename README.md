# Tempora Portal
A react.js app, based off Facebook's create-react-app bootstrapper

## To run:
1. npm install create-react-app
1. npm install
1. npm start


## Signing In

To login as a patient:
- username: mulder
- password: testpw1
To login as a doctor:
- username: scully
- password: testpw1

On successful login, the API will send back an encrypted JSON Web Token that is stored in LocalStorage. Every request outside of /auth is protected via JWT verification.

## Using The App

- On load, the app will prompt you to login.
- On successful login, if you are a doctor, you will see a list of patients, along with a search bar to filter the results. Each item in the list links to that patient's profile.
- A patient's profile looks largely the same in the patient and doctor views, though there is an "accept" button on appointments if the logged in user is a doctor.
- To schedule a new appointment, click "New Appointment" and fill out the form.
- To upload a file, click "Choose File", select one from your computer, then click "Upload File".
- To remove a file or cancel an appointment, click "Remove" or "Cancel", respectively.
- To download a file, click the download icon on the file.
