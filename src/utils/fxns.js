export { camelToSpace }

function camelToSpace(str) {
  return str.replace( /([A-Z])/g, " $1" )
}
