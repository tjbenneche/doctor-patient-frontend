import axios from 'axios'

axios.defaults.headers.post['Content-Type'] = 'application/json'

const BASE_URL = 'http://localhost:1123'

export {getPatientData, postAppointment, createSession, getPatientsData, getPatientAppointments, getDoctors, postFile, getPatientFiles, deleteAppointment, deleteFile, patchAppointment}

// Login Requests
function createSession(body) {
  const url = `${BASE_URL}/auth/login`
  return axios.post(url, JSON.stringify(body)).then(response => response.data)
}
// Patient Requests
function getPatientsData() {
  const url = `${BASE_URL}/patients?jwt=${localStorage.getItem('jwt')}`
  return axios.get(url).then(response => response.data)
}
function getPatientData(id, type) {
  const url = `${BASE_URL}/patients/${id}?idType=${type}&jwt=${localStorage.getItem('jwt')}`
  return axios.get(url).then(response => response.data)
}
// Appointment Requests
function getPatientAppointments(id, doctorId) {
  let url = `${BASE_URL}/patients/` + id + `/appointments?jwt=${localStorage.getItem('jwt')}`
  if (doctorId) {
    url += `&doctorId=${doctorId}`
  }
  return axios.get(url).then(response => response.data)
}
function postAppointment(body) {
  const url = `${BASE_URL}/appointments?jwt=${localStorage.getItem('jwt')}`
  return axios.post(url, JSON.stringify(body)).then(response => response.data)
}
function patchAppointment(id, body) {
  const url = `${BASE_URL}/appointments/${id}?jwt=${localStorage.getItem('jwt')}`
  return axios.patch(url, body).then(response => response.data)
}
function deleteAppointment(id) {
  const url = `${BASE_URL}/appointments/${id}?jwt=${localStorage.getItem('jwt')}`
  return axios.delete(url).then(response => response.data)
}
// File Requests
function getPatientFiles(id) {
  const url = `${BASE_URL}/patients/` + id + `/files?jwt=${localStorage.getItem('jwt')}`
  return axios.get(url).then(response => response.data)
}
function postFile(body) {
  const url = `${BASE_URL}/files?jwt=${localStorage.getItem('jwt')}`
  return axios.post(url, JSON.stringify(body)).then(response => response.data)
}
function deleteFile(id) {
  const url = `${BASE_URL}/files/${id}?jwt=${localStorage.getItem('jwt')}`
  return axios.delete(url).then(response => response.data)
}
// Doctor Requests
function getDoctors() {
  const url = `${BASE_URL}/doctors?jwt=${localStorage.getItem('jwt')}`
  return axios.get(url).then(response => response.data)
}
