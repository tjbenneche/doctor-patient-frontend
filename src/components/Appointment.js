import React, { Component } from 'react'
import {postAppointment, getDoctors} from '../utils/temporaApi'


class Appointment extends Component {
  constructor(props) {
    super(props)
    this.state = {datetime: '', purpose: '', doctorName: '', patientId: '', doctorId: '', status: '', doctors: []}
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleDoctorDropdown = this.handleDoctorDropdown.bind(this)
    this.handleBack = this.handleBack.bind(this)
  }
  // Lifecycle Hooks
  componentDidMount() {
    if (localStorage.getItem('userType') === 'doctor') {
      this.setState({doctorId: localStorage.getItem('userId')})
    }
    this.listDoctors()
  }
  // Event Handlers
  handleBack() {
    this.props.history.push('/')
  }
  handleChange(e) {
    const target = e.target
    const value = target.value
    const name = target.name
    this.setState({
      [name]: value
    })
  }
  handleDoctorDropdown(e) {
    const target = e.target
    const value = target.value
    const name = target[target.selectedIndex].text
    this.setState({
      doctorId: value,
      doctorName: name
    })
  }
  handleSubmit(e) {
    e.preventDefault()
    this.createAppointment({
      datetime: this.state.datetime,
      purpose: this.state.purpose,
      doctorName: this.state.doctorName,
      doctorId: this.state.doctorId,
      patientId: localStorage.getItem('patientId'),
      status: localStorage.getItem('userType') === 'doctor' ? 'accepted' : 'pending'
    })
  }
  // Methods
  listDoctors() {
    getDoctors().then((doctors) => {
      this.setState({doctors: doctors.data, doctorId: doctors.data[0]._id, doctorName: doctors.data[0].username})
    })
  }
  createAppointment(body) {
    postAppointment(body).then((val) => {
      // #FIXME if doctor send back to patient page
      this.props.history.push('/')
    })
  }
  render() {

    const { doctors } = this.state

    return (
      <div className="form-container">
        <form onSubmit={this.handleSubmit}>
          <label>Doctor</label>
          <select name="doctor" value={this.state.doctorId} onChange={this.handleDoctorDropdown}>
          {
            doctors.map((doctor, index) => (
              <option key={index} value={doctor._id}>{doctor.username}</option>
            ))
          }
          </select>
          <label>Purpose</label>
          <input name="purpose" value={this.state.purpose} onChange={this.handleChange}/>
          <label>Date</label>
          <input type="datetime-local" name="datetime" value={this.state.datetime} onChange={this.handleChange}/>
          <input className="primary-btn" type="submit" value="Schedule Appointment" />
          <button className="secondary-btn" onClick={this.handleBack}>Cancel</button>
        </form>
      </div>
    )
  }
}

export default Appointment
