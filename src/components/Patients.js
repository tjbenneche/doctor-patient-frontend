import React, { Component } from 'react'
import SearchInput, {createFilter} from 'react-search-input'
import { getPatientsData } from '../utils/temporaApi'

const KEYS_TO_FILTERS = ['characteristics.name']


class Patients extends Component {

  constructor() {
    super()
    this.state = { patients: [], searchTerm: '' }
    this.searchUpdated = this.searchUpdated.bind(this)
  }
  // Lifecycle Hooks
  componentDidMount() {
    this.getPatients()
  }
  // Event Handlers
  searchUpdated (term) {
    this.setState({searchTerm: term})
  }
  // Methods
  getPatients() {
    getPatientsData().then((patients) => {
      this.setState({ patients: patients.data })
    })
  }
  render() {

    const { patients }  = this.state
    const filteredPatients = patients.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

    return (
      <section>
        <h1>Patients List</h1>
        <SearchInput className="search-input" onChange={this.searchUpdated} />
        <div className="patients-container">
          {filteredPatients.map((patient, index) => (
            <a href={'/patients/' + patient._id} key={index}>
              <div className="patient-box">
                <span className="patient-char-label">Name</span>
                <span className="patient-char">{patient.characteristics.name}</span>
                <span className="patient-char-label">Age</span>
                <span className="patient-char">{patient.characteristics.age}</span>
              </div>
            </a>
            )
          )}
        </div>
      </section>
    )
  }
}

export default Patients
