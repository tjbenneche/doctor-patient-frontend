import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { getPatientAppointments, deleteAppointment, patchAppointment } from '../utils/temporaApi'

class DoctorOptions extends Component {
  render() {
    if (localStorage.getItem('userType') === 'doctor' && this.props.status === 'pending') {
      return (
        <button className="accept-btn" onClick={() => this.props.thisAppointment.acceptAppointment(this.props.appointmentId, {status: 'accepted'})}>Accept</button>
      )
    } else {
      return(null)
    }
  }
}

class Appointments extends Component {

  constructor() {
    super()
    this.state = { appointments: [] }
    this.cancelAppointment = this.cancelAppointment.bind(this)
    this.acceptAppointment = this.acceptAppointment.bind(this)
    this.formatDate = this.formatDate.bind(this)
  }
  // Lifecycle Hooks
  componentDidMount() {
    this.getAppointments()
  }
  // Event Handlers
  cancelAppointment(id) {
    deleteAppointment(id).then((appointment) => {
      this.getAppointments()
    })
  }
  acceptAppointment(id, body) {
    patchAppointment(id, body).then((appointment) => {
      this.getAppointments()
    })
  }
  // Methods
  getAppointments() {
    let doctorId = localStorage.getItem('userType') === 'doctor' ? localStorage.getItem('userId') : null
    getPatientAppointments(localStorage.getItem('patientId'), doctorId).then((appointments) => {
      this.setState({ appointments: appointments.data })
    })
  }
  formatDate(date) {
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    let d = new Date(date)
    let hours = d.getUTCHours()
    let minutes = d.getUTCMinutes()
    let ampm = 'AM'
    if (hours > 11) {
      ampm = 'PM'
    }
    if (hours > 12) {
      hours -= 12
    }
    if (hours === 0) {
      hours = 12
    }
    if (minutes === 0) {
      minutes = '00'
    }
    return days[d.getUTCDay()] + ' ' +  months[d.getUTCMonth()] + ' ' + d.getUTCDate() + ', ' + d.getUTCFullYear() + ' ' + hours + ':' + minutes + ' ' + ampm
  }
  render() {

    const { appointments }  = this.state

    return (
      <section className="appointments">
        <h2>Appointments</h2>
        <Link to="/appointments/new" className="primary-btn">New Appointment</Link>
        {
          appointments.map((appointment, index) => (
              <div className="appointment-box" key={index}>
                <span className={"appointment-title " + appointment.status}>{'Appointment with ' + appointment.doctorName}</span>
                <div className="appointment-details">
                  <span className="appointment-datetime">{this.formatDate(appointment.datetime)}</span>
                  <span className="appointment-status">{appointment.status}</span>
                  <p className="appointment-purpose">{appointment.purpose}</p>
                  <div className="appointment-actions">
                    <button className="delete-btn" onClick={() => this.cancelAppointment(appointment._id)}>Cancel</button>
                    <DoctorOptions appointmentId={appointment._id} thisAppointment={this} status={appointment.status}/>
                  </div>
                </div>
              </div>
            )
          )
        }
      </section>
    )
  }
}

export default Appointments
