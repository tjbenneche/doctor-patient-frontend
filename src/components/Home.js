import React, { Component } from 'react'
import Patients from './Patients'
import Patient from './Patient'

class Home extends Component {

  // Methods
  isLoggedIn() {
    return localStorage.getItem('jwt')
  }
  isDoc() {
    return localStorage.getItem('userType')
  }
  renderView() {
    console.log(this.isDoc())
    if (this.isLoggedIn()) {
      if (this.isDoc() === 'doctor') {
        return <Patients />
      } else {
        return (
          <Patient />
        )
      }
    } else {
      return (
        <p>Please login using the link in the menu bar.</p>
      )
    }
  }
  render() {
    return (
      <div>
        {this.renderView()}
      </div>
    )
  }
}

export default Home
