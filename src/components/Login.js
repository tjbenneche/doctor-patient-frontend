import React, { Component } from 'react'
import {createSession} from '../utils/temporaApi'

class LoginForm extends Component {

  constructor(props) {
    super(props)
    this.state = {username: '', password: ''}
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  // Event Handlers
  handleChange(e) {
    const target = e.target
    const value = target.value
    const name = target.name
    this.setState({
      [name]: value
    })
  }
  handleSubmit(e) {
    e.preventDefault()
    this.createSession({username: this.state.username, password: this.state.password})
  }
  // Methods
  createSession(body) {
    createSession(body).then((val) => {
      localStorage.setItem('jwt', val.included.jwt)
      localStorage.setItem('userType', val.data.type)
      localStorage.setItem('userId', val.data._id)
      this.props.history.push('/')
    })
  }
  render() {
    return (
      <div className="form-container">
        <form onSubmit={this.handleSubmit}>
          <label>Username</label>
          <input name="username" value={this.state.username} onChange={this.handleChange}/>
          <label>Password</label>
          <input type="password" name="password" value={this.state.password} onChange={this.handleChange}/>
          <input type="submit" value="Login" />
        </form>
      </div>
    )
  }
}

export default LoginForm
