import React, { Component } from 'react'

class Logout extends Component {

  // Lifecycle Hooks
  componentDidMount() {
    localStorage.removeItem('jwt')
    this.props.history.push('/login')
  }
  // Methods
  render() {
    return (
      <p>You have been logged out, you will be redirected shortly to login...</p>
    )
  }
}

export default Logout
