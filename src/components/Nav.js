import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../styles/App.css'

class Nav extends Component {

  // Methods
  render() {
    let loginLogout = null
    if (localStorage.getItem('jwt')) {
      loginLogout = <li><Link to="/logout">Logout</Link></li>
    } else {
      loginLogout = <li><Link to="/login">Login</Link></li>
    }
    return (
      <header>
        <Link to="/"><span className="logo">Tempora Portal</span></Link>
        <nav>
          <ul>
            <li><Link to="/">Home</Link></li>
            {loginLogout}
          </ul>
        </nav>
      </header>
    )
  }
}

export default Nav
