import React, { Component } from 'react'
import { getPatientFiles, postFile, deleteFile } from '../utils/temporaApi'

class Files extends Component {

  constructor() {
    super()
    this.state = { files: [], file: {} }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleFileUpload = this.handleFileUpload.bind(this)
    this.removeFile = this.removeFile.bind(this)
  }
  // Lifecycle Hooks
  componentDidMount() {
    this.listFiles()
  }
  // Event Handlers
  removeFile(id) {
    deleteFile(id).then((file) => {
      this.listFiles()
    })
  }
  handleSubmit(e) {
    e.preventDefault()
    let fr = new FileReader()
    fr.onloadend = (e) => {
      this.uploadFile({
        file: fr.result,
        name: this.state.file.name,
        patientId: localStorage.getItem('patientId'),
        type: this.state.file.type
      })
    }
    fr.readAsDataURL(this.state.file)
  }
  handleFileUpload(e) {
    const file = e.target.files[0]
    this.setState({ file: file })
  }
  // Methods
  listFiles() {
    getPatientFiles(localStorage.getItem('patientId')).then((files) => {
      this.setState({ files: files.data })
    })
  }
  uploadFile(body) {
    postFile(body).then((file) => {
      this.listFiles()
    })
  }
  render() {

    const { files }  = this.state

    return (
      <section className="file-section">
        <h2>Medical Records</h2>
        <form onSubmit={this.handleSubmit} encType="multipart/form-data">
          <input name="file" type="file" onChange={this.handleFileUpload} />
          <input type="submit" className="primary-btn" value="Upload File" />
        </form>
        <div className="files">
          {
            files.map((file, index) => (
                <div className="file-box" key={index}>
                  <span className="file-name">{file.name}</span>
                  <div className="file-actions">
                    <a href={file.data.content}  download={file.name}><button className="download-btn">⇩</button></a>
                    <button className="delete-btn" onClick={() => this.removeFile(file._id)}>Remove</button>
                  </div>
                  <span className="file-corner"></span>
                </div>
              )
            )
          }
        </div>
      </section>
    )
  }
}

export default Files
