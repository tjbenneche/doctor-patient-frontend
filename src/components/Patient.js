import React, { Component } from 'react'
import { getPatientData } from '../utils/temporaApi'
import Appointments from './Appointments'
import Files from './Files'
import {camelToSpace} from '../utils/fxns'

class PatientCharacteristicBox extends Component {
  render() {
    let charDivs = []
    let chars = this.props.chars
    for (var char in chars) {
      charDivs.push(<PatientCharacteristic name={char} val={chars[char]} key={char} />)
    }
    return (
      <div className="patient-char-box">
        <h2>Patient Information</h2>
        {charDivs}
      </div>
    )
  }
}

class PatientCharacteristic extends Component {
  render() {
    return (
      <div className="patient-char">
        <span className="patient-char-name">{camelToSpace(this.props.name)}</span>
        <span className="patient-char-val">{this.props.val}</span>
      </div>
    )
  }
}

class Patient extends Component {

  constructor() {
    super()
    this.state = {patient: {characteristics: []}}
  }
  // Lifecycle Hooks
  componentDidMount() {
    this.getPatient()
  }
  // Methods
  getPatient() {
    let id = ''
    let type = ''
    if (localStorage.getItem('userType') === 'doctor') {
      let path = this.props.location.pathname
      id = path.substr(path.length - 24)
      type = 'patient'
    } else {
      id = localStorage.getItem('userId')
      type = 'user'
    }
    getPatientData(id, type).then((patient) => {
      localStorage.setItem('patientId', patient.data._id)
      this.setState({patient: patient.data})
    })
  }
  render() {

    const { patient } = this.state

    return (
      <div>
        <h1>{patient.characteristics.name}</h1>
        <div className="patient-container">
          <section className="patient-info">
            <PatientCharacteristicBox chars={patient.characteristics} />
          </section>
          <Appointments />
          <Files />
        </div>
      </div>
    )
  }
}

export default Patient
