import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Nav from './components/Nav'
import LoginForm from './components/Login'
import Home from './components/Home'
import Logout from './components/Logout'
import Patient from './components/Patient'
import Appointment from './components/Appointment'
import './styles/App.css'

const Root = () => {
  return (
    <Router>
      <div>
        <Nav />
        <main>
          <Route exact path='/' component={Home}/>
          <Route path='/login' component={LoginForm}/>
          <Route path='/patients/:id' component={Patient}/>
          <Route path='/logout' component={Logout}/>
          <Route path='/appointments/new' component={Appointment}/>
        </main>
      </div>
    </Router>
  )
}


ReactDOM.render(<Root />, document.getElementById('root'))
